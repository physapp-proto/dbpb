#!/bin/bash
set -e

rm -rf *pb.go

service=db
protoc --go_out=../ --go-grpc_out=../ ${service}/${service}pb.proto

# Function to increment the last digit of the version tag
increment_version() {
    local version="$1"
    local major_version=$(echo "$version" | cut -d. -f1)
    local minor_version=$(echo "$version" | cut -d. -f2)
    local patch_version=$(echo "$version" | cut -d. -f3)
    patch_version=$((patch_version+1))
    echo "${major_version}.${minor_version}.${patch_version}"
}

if [ -z "${1}" ]; then
    if [ ! -f "tags" ] || [ ! -s "tags" ]; then
        # If the 'tags' file doesn't exist or is empty, set the first tag as v0.0.0
        echo "v0.0.0"
    else
        # Extract the last tag
        last_tag=$(tail -n 1 tags)
        # Remove the 'v' prefix
        last_tag_without_v="${last_tag#v}"
        # Increment the last digit
        new_tag_without_v=$(increment_version "$last_tag_without_v")
        # Add the 'v' prefix back
        tagVersion="v$new_tag_without_v"
    fi
else
    tagVersion="${1}"
fi

if [[ "${1}" = "prod" ]]; then
    if [ ! -f "tags" ] || [ ! -s "tags" ]; then
        # If the 'tags' file doesn't exist or is empty, set the first tag as v0.0.0
        echo "v0.0.0"
    else
        # Extract the last tag
        last_tag=$(tail -n 1 tags)
        # Remove the 'v' prefix
        last_tag_without_v="${last_tag#v}"
        # Increment the last digit
        new_tag_without_v=$(increment_version "$last_tag_without_v")
        # Add the 'v' prefix back
        tagVersion="v$new_tag_without_v"
    fi
    
    echo "${tagVersion}" >> tags
    go mod init && go mod tidy

    git add .
    git commit -m ${tagVersion}
    git push
    git tag ${tagVersion}
    git push origin --tags

    go install gitlab.com/physapp-proto/${service}pb@${tagVersion}
fi